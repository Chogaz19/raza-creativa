import {functions} from './config'
const nodemailer = require('nodemailer')

exports.enviar = 
functions.https.onCall( ({titulo, cuerpo, asunto, correo_electronico, configuracion_de_correo}:any, context:any) => 

new Promise<void>( async (resolve, rejected )=>{ 
  try{
    let transporter = nodemailer.createTransport(configuracion_de_correo)
    let texto =`<div>
            <h4>${titulo}</h4>
            <h4>Mensaje: </h4>
            <p>${cuerpo}</p>
        </div>`
    const mailOptions = {
      from: configuracion_de_correo.auth.user, 
      to: correo_electronico,
      subject: asunto,
      html: texto,
    }
    await transporter.sendMail(mailOptions)
    resolve()
  } catch ( e ) {
    console.log(configuracion_de_correo)
    rejected( e )
  }
}))