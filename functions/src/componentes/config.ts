const firebase = require("firebase-admin")
const functions = require('firebase-functions');

firebase.initializeApp(functions.config().firebase);
export {
  firebase,
  functions,
}