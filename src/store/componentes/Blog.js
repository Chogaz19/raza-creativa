import Vue from 'vue'
import Vuex from 'vuex'
import { Colección_blog } from '@/FirebaseConfig'
import { getDocs, onSnapshot, orderBy, query, where } from 'firebase/firestore'
Vue.use(Vuex)

export default {
  namespaced: true,
  state: {
    registro: [],
    cantidad_de_registros: 0,
    entrada: {
      contenido: '',
      título: '',
      subtítulo: '',
      portada: '',
      imágenes: [],
      etiquetas: []
    },
  },
  actions: {
    obtener_por_slug: ( context, _slug)=> new Promise((resolve, reject)=>{
      let coleccion = []
      const _query = query(Colección_blog, where('slug', '==', _slug), orderBy('creado_el', 'desc'))
      getDocs(_query).then(({docs})=>{
        docs.map( doc => coleccion.push({...doc.data(), id: doc.id, creado_el: doc.data().creado_el.toDate() }))
        let objeto = coleccion.length===0 ? {} : coleccion[0]
        resolve(objeto)
      }).catch(error=>{
        console.error(error)
        reject(error)
      })
    }),
    obtener_todos: ({state})=>{
      onSnapshot(Colección_blog, ({docs, size})=> {
        const registro = []
        docs.map( doc => registro.push({...doc.data(), id: doc.id, creado_el: doc.data().creado_el.toDate() }))
        state.registro = registro
        state.cantidad_de_registros = size
      })
    },
    obtener_por_id_de_blog: async( {state, dispatch}, _id ) =>  {
      if(state.registro.length==0) await dispatch('obtener_todos')
      state.entrada = await dispatch('obtener_por_id', _id)
    },
    obtener_por_id: async( {state}, _id ) =>  {
      return state.registro.filter(({id})=>id==_id)[0]
    },
    obtener_por_slug_de_blog: async ({state, dispatch}, _slug) => {
      if(state.registro.length==0) await dispatch('obtener_todos')
      dispatch('obtener_por_slug', _slug).then(data=>state.entrada = data)
    },
  },
  getters: {
    registro: ({registro}) => registro,
    cantidad_de_registros: ({cantidad_de_registros}) => cantidad_de_registros,
    entrada: ({entrada}) => entrada || {},
    registro_de_entradas: ({registro})=>({filtro, etiqueta_filtro})=>{
      if(!etiqueta_filtro&&typeof etiqueta_filtro == 'boolean') {
        if(filtro=='') return registro
        else return registro.filter(({título})=>título.toUpperCase().includes(filtro.toUpperCase()))
      } else {
        if(filtro=='') return registro.filter(({etiquetas})=>etiquetas.some(({id})=>id==etiqueta_filtro))
        else return registro.filter(({etiquetas})=>etiquetas.some(({id})=>id==etiqueta_filtro)).filter(({título})=>título.toUpperCase().includes(filtro.toUpperCase()))
      }
    },
    registro_de_entradas_por_etiquetas: ({registro})=>(id_etiqueta)=>{
      return registro.filter(({etiquetas})=>etiquetas.some(({id})=>id==id_etiqueta)) 
    },
    registro_de_segundo_a_cuarto: ({registro})=>registro.slice(1, 5),
    registro_de_primero_a_segundo: ({registro})=>registro.slice(0, 2),
  },

}